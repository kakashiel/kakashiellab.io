const config = {
	verbose: true,
	rootDir: '../..',
	setupFiles: ['<rootDir>/src/__mocks__/setupJestMock.js'],
	roots: ['<rootDir>/src/'],
	testEnvironment: 'jsdom',
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
	transform: {
		'^.+\\.(ts|tsx)$': 'ts-jest',
	},
	transformIgnorePatterns: ['/node_modules/'],
	testRegex: '.*\\.test\\.(ts|tsx)$',
	moduleDirectories: ['node_modules'],
	moduleNameMapper: {
		'\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|md)$':
			'<rootDir>/src/__mocks__/fileMock.js',
		'\\.(css|less)$': '<rootDir>/src/__mocks__/fileMock.js',
	},
	coverageReporters: ['text'],
	coverageDirectory: '<rootDir>/configs/tests/__coverage__/',
	coverageThreshold: {
		global: {
			branches: 100,
			functions: 100,
			lines: 100,
			statements: 100,
		},
	},
	collectCoverageFrom: [
		'**/*.{ts,tsx}',
		'!**/node_modules/**',
		'!src/i18n/index.tsx',
		'!src/setupTests.ts',
		'!src/index.tsx',
		'!src/react-app-env.d.ts',
		'!src/serviceWorker.ts',
	],
};

module.exports = config;

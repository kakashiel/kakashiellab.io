This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Features
Front:
- Atomic architecture
- eslint/prettier
- typescript
- precommit hook static analyse
- gitlab ci/cd
- Minalmal use of libraries
- User react context
- Google Oauth2 authentification
- login/logout
- expire token handleing
- Unit tests 70%coverage
- Error unauthorised handling
- Axios instance

## Thanks to
The theme used is adapted from 
http://themes.gohugo.io/beautifulhugo/.
https://beautifuljekyll.com/

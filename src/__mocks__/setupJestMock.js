global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};

global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({ mock: { } }),
      text: () => Promise.resolve('I\'m a mock find me!')
    })
);
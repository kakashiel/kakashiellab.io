import useTranslation from './index';

describe('should test usetranslation hook', () => {
	it('should return entry if namespace doesnt exist', () => {
		const { t } = useTranslation('nsWhichDoesntExist');
		expect(t('text')).toBe('text');
	});

	it('should return entry if text doesnt exist', () => {
		const { t } = useTranslation('footer');
		expect(t('text')).toBe('text');
	});

	it('should return translation ', () => {
		const { t } = useTranslation('header');
		expect(t('title')).toBe('Damn kakashiel');
	});
});

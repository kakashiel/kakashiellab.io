import * as React from 'react';
import { render } from '@testing-library/react';
import PagesWrapper from './index';
import { BrowserRouter } from 'react-router-dom';

jest.mock('../../atoms/MarkdownViewer');

describe('Test PagesWrapper', () => {
	it('should test if compo truthy', async () => {
		const compo = render(
			<PagesWrapper>
				<div />
			</PagesWrapper>,
			{ wrapper: BrowserRouter },
		);
		expect(compo).toBeTruthy();
	});
});

import { render } from '@testing-library/react';
import PagesWrapper from '../PagesWrapper';
import * as React from 'react';
import PathfinderVisualizer from './index';
import { ArrayContextProvider } from './ArrayContext';

describe('Test PagesWrapper', () => {
	it('should test if compo truthy', async () => {
		const compo = render(
			<ArrayContextProvider>
				<PathfinderVisualizer />
			</ArrayContextProvider>,
		);
		expect(compo).toBeTruthy();
	});
});

import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Home from './index';

describe('Test home page', () => {
	it('should render page', async () => {
		const compo = render(<Home />, { wrapper: BrowserRouter });
		await waitFor(() => expect(compo).toBeTruthy());
	});
});

import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import ListChallenges from './index';

describe('Test list page', () => {
	it('should render page', async () => {
		const compo = render(<ListChallenges />, { wrapper: BrowserRouter });
		await waitFor(() => expect(compo).toBeTruthy());
	});
});

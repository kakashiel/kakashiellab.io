import { screen, fireEvent, render } from '@testing-library/react';
import * as React from 'react';
import Cell from './index';

describe('Test PagesWrapper', () => {
	it('should test if compo truthy', async () => {
		const array = [
			[1, 0],
			[1, 2],
		];
		const compo = render(
			<Cell
				y={0}
				x={0}
				setDragging={() => {}}
				dragging={{ caseType: 1, x: 0, y: 0 }}
				cellType={1}
				array={array}
				forceRender={0}
			/>,
		);
		expect(compo).toBeTruthy();
		fireEvent.click(screen.getByTestId('cell-0-0'));
		expect(array[0][0]).toBe(1);
	});
});

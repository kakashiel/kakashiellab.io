import React from 'react';
import { render } from '@testing-library/react';
import Header from './index';
import { BrowserRouter } from 'react-router-dom';

jest.mock('../../atoms/MarkdownViewer');

describe('Should test header', () => {
	it('Should mount header', () => {
		const component = render(<Header />, { wrapper: BrowserRouter });
		expect(component).toBeTruthy();
	});
});

export {};

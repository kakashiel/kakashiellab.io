import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import HeaderContainer from './index';
import { BrowserRouter } from 'react-router-dom';

describe('Should test header', () => {
	it('Should not have class smaller', async () => {
		const { container } = render(<HeaderContainer />, { wrapper: BrowserRouter });
		await waitFor(() => expect(container.querySelector('.smaller')).toBeFalsy());
	});

	it('Should have class smaller if scroll', async () => {
		document.documentElement.scrollTop = 250;
		const { container } = render(<HeaderContainer />, { wrapper: BrowserRouter });
		await waitFor(() => expect(container.querySelector('#header')).toBeTruthy());
		fireEvent.scroll(window, { target: { scrollY: 160 } });
		await waitFor(() => expect(container.querySelector('.header-smaller')).toBeTruthy());
	});

	it('Should not have class smaller if scroll under 150', async () => {
		document.documentElement.scrollTop = 100;
		const { container } = render(<HeaderContainer />, { wrapper: BrowserRouter });
		await waitFor(() => expect(container.querySelector('#header')).toBeTruthy());
		fireEvent.scroll(window, { target: { scrollY: 160 } });
		await waitFor(() => expect(container.querySelector('.header-smaller')).toBeFalsy());
	});
});

export {};

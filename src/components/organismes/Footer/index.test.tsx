import * as React from 'react';
import { render } from '@testing-library/react';
import Footer from './index';
import { BrowserRouter } from 'react-router-dom';

jest.mock('../../atoms/MarkdownViewer');
describe('Test body page', () => {
	it('should render page', () => {
		const compo = render(<Footer />, { wrapper: BrowserRouter });
		expect(compo).toBeTruthy();
	});
});

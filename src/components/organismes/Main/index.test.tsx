import * as React from 'react';
import { render } from '@testing-library/react';
import Main from './index';

jest.mock('../../atoms/MarkdownViewer');
describe('Test body page', () => {
	it('should render page', () => {
		const compo = render(
			<Main>
				<div />
			</Main>,
		);
		expect(compo).toBeTruthy();
	});
});

import * as React from 'react';
import { render } from '@testing-library/react';
import Article from './index';

jest.mock('../../atoms/MarkdownViewer');

describe('Test article page', () => {
	it('should render page', async () => {
		const compo = render(<Article file={''} />);
		expect(compo).toBeTruthy();
	});
});

export {};

import * as React from 'react';
import './styles.css';

const ButtonCircle: React.FC<Props> = ({ text, onClick }) => {
	return (
		<button className={'button-circle'} type={'button'} onClick={onClick}>
			{text}
		</button>
	);
};

type Props = {
	text: string;
	onClick: (e: React.MouseEvent<HTMLElement>) => void;
};

export default ButtonCircle;

import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import ButtonCircle from './index';

describe('Test button', () => {
	it('should display button', async () => {
		const { queryByText } = render(<ButtonCircle text={'go'} onClick={() => {}} />);
		await waitFor(() => expect(queryByText('go')).toBeTruthy());
	});
});

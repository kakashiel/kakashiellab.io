import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import SeparatorVertical from './index';

describe('Test SeparatorVertical', () => {
	it('should display SeparatorVertical', async () => {
		const { queryByTestId } = render(<SeparatorVertical />);
		await waitFor(() => expect(queryByTestId('SeparatorVertical')).toBeTruthy());
	});
});

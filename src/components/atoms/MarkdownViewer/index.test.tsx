import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import MarkdownViewer from './index';

jest.mock('marked', () => ({
	marked: () => 'markdownText',
}));

describe('Test markdownviewer', () => {
	it('should display markdownviewer', async () => {
		const fakeFetch = jest.fn().mockReturnValue({ text: jest.fn() });
		window.fetch = fakeFetch;

		const { queryByText } = render(<MarkdownViewer file={'go'} />);
		await waitFor(() => expect(queryByText('markdownText')).toBeTruthy());
		await waitFor(() => expect(fakeFetch).toHaveBeenCalledTimes(1));
	});
});

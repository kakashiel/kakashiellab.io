import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import Dropdown from './index';

describe('Test Dropdown', () => {
	it('should display button', async () => {
		const { queryByText } = render(<Dropdown text={'go'}><div></div></Dropdown>);
		await waitFor(() => expect(queryByText('go')).toBeTruthy());
	});
});

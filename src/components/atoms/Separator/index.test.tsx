import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import Separator from './index';

describe('Test Separator', () => {
	it('should display Separator', async () => {
		const { queryByTestId } = render(<Separator />);
		await waitFor(() => expect(queryByTestId('Separator')).toBeTruthy());
	});
});

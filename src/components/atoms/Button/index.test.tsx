import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import Button from './index';

describe('Test button', () => {
	it('should display button', async () => {
		const { queryByText } = render(<Button text={'go'} onClick={() => {}} />);
		await waitFor(() => expect(queryByText('go')).toBeTruthy());
	});
});

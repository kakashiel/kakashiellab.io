import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import InputRadio from './index';

describe('Test InputRadio', () => {
	it('should display InputRadio', async () => {
		const { queryByText, queryByTestId } = render(
			<InputRadio text={'go'} value={''} id={''} onChange={() => {}} />,
		);
		await waitFor(() => expect(queryByText('go')).toBeTruthy());
		await waitFor(() => expect(queryByTestId('InputRadio-container')).toBeTruthy());
	});
});

import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import InputRange from './index';

describe('Test InputRange', () => {
	it('should display InputRange', async () => {
		const { queryByText } = render(
			<InputRange text={'go'} value={1} min={1} max={3} id={''} onChange={() => {}} />,
		);
		await waitFor(() => expect(queryByText('go')).toBeTruthy());
	});
});

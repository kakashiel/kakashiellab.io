import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import Cross from '.';

describe('Test Menu', () => {
	it('should display Menu', async () => {
		const { queryByTestId } = render(<Cross />);
		await waitFor(() => expect(queryByTestId('Cross')).toBeTruthy());
	});
});

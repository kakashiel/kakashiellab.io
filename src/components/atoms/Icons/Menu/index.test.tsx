import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import Menu from '../Menu';

describe('Test Menu', () => {
	it('should display Menu', async () => {
		const { queryByTestId } = render(<Menu />);
		await waitFor(() => expect(queryByTestId('Menu')).toBeTruthy());
	});
});

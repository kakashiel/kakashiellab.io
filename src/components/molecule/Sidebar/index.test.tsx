import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import Sidebar from './index';

describe('Test Sidebar', () => {
	it('should display InputRange', async () => {
		const { queryByTestId } = render(
			<Sidebar>
				<div></div>
			</Sidebar>,
		);
		await waitFor(() => expect(queryByTestId('Sidebar')).toBeTruthy());
	});
});

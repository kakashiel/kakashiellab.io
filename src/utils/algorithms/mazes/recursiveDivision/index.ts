import { CASE_TYPE } from '../../../../components/pages/PathfinderVisualizer/ArrayContext';
import { Vector } from '../../../../type/math';
import { getRandomEvenNumber, getRandomOddNumber } from '../../../number';

export function* recursiveDivision(array: number[][]): IterableIterator<{ [s: number]: number[] }> {
	function* verticalWall(array: number[][], w: Vector, h: Vector) {
		const rangeWall = getRandomOddNumber(w.x, w.y);
		const door = getRandomEvenNumber(h.x, h.y);
		for (let i = w.x; i < w.y; i++) {
			console.log(`vertical w: ${w} | h: ${h} | i:${i}   ${rangeWall}`);
			if (i !== door && array[i][rangeWall] === CASE_TYPE.EMPTY) array[i][rangeWall] = CASE_TYPE.WALL;
		}
		return rangeWall;
	}
	function* horizontalWall(array: number[][], w: Vector, h: Vector) {
		const rangeWall = getRandomOddNumber(w.x, w.y);
		const door = getRandomEvenNumber(h.x, h.y);
		for (let i = h.x; i < h.y; i++) {
			console.log(`horizontal w: ${w.x},${w.y} | h: ${h.x},${h.y} | i:${i}   ${rangeWall}`);
			if (i !== door && array[rangeWall][i] === CASE_TYPE.EMPTY) array[rangeWall][i] = CASE_TYPE.WALL;
		}
		return rangeWall;
	}
	function* startRecursiveDivision(array: number[][], w: Vector, h: Vector): any {
		const WEIGHT = 10;
		const wSize = w.y - w.x;
		const hSize = h.y - h.x;
		const isVerticalWall = wSize > WEIGHT;
		console.log(`w: ${wSize}   h:${hSize}`);
		if (wSize < 1 || hSize < 1) {
			console.log('should return');
			return;
		}
		yield {};
		if (isVerticalWall) {
			const rangeWall = yield* verticalWall(array, w, h);
			yield* startRecursiveDivision(array, { x: w.x, y: rangeWall - 1 }, { x: h.x, y: h.y });
			yield* startRecursiveDivision(array, { x: rangeWall + 1, y: w.y }, { x: h.x, y: h.y });
		} else {
			const rangeWall = yield* horizontalWall(array, w, h);
			yield* startRecursiveDivision(array, { x: w.x, y: w.y }, { x: h.x, y: rangeWall - 1 });
			yield* startRecursiveDivision(array, { x: w.x, y: w.y }, { x: rangeWall + 1, y: h.y });
		}
	}

	console.log(array);
	yield* startRecursiveDivision(array, { x: 0, y: array.length }, { x: 0, y: array[0].length });
}

import { generateArrayRandomNumber } from '../../number';
import { quickSort } from './index';

describe('Test merge sort algo', () => {
	it('First test', () => {
		const array = [4, 1, 3, 2];
		for (const mergeSortElement of quickSort(array)) {
		}
		expect(array).toStrictEqual([1, 2, 3, 4]);
	});

	it('Empty array test', () => {
		const array: number[] = [];
		for (const mergeSortElement of quickSort(array)) {
		}
		expect(array).toStrictEqual([]);
	});

	it('Randome array test', () => {
		const array = generateArrayRandomNumber(1000, 500);
		for (const mergeSortElement of quickSort(array)) {
		}
		expect(array.every((v, i, a) => !i || a[i - 1] <= v)).toBeTruthy();
	});
});

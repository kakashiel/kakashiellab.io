import React from 'react';
import { createRef } from 'react';
import { fireEvent, render, screen, renderHook } from '@testing-library/react';

import useOnClickOutside from './index';

describe('useOnClickOutside', () => {
	it('calls handler when click is outside element', () => {
		// Arrange
		const handler = jest.fn();
		const ref = createRef<HTMLDivElement>();
		render(<div ref={ref}></div>);

		// Act
		renderHook(() => useOnClickOutside(ref, handler));
		fireEvent.mouseDown(document);

		// Assert
		expect(handler).toBeCalledTimes(1);
	});

	it(`doesn't calls handler when click is within element`, () => {
		// Arrange
		const handler = jest.fn();
		const ref = createRef<HTMLDivElement>();
		render(<div ref={ref} data-testid="element-testid"></div>);

		// Act
		renderHook(() => useOnClickOutside(ref, handler));
		fireEvent.mouseDown(screen.getByTestId('element-testid'));

		//  Assert
		expect(handler).not.toBeCalled();
	});
});

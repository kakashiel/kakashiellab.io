import { getRandomArbitrary } from './index';

describe('Test number utils', () => {
	it('Generate random number', () => {
		expect(getRandomArbitrary(1, 1)).toBe(1);
	});
});

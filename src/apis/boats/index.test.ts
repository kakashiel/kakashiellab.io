import { getAllBoats, getBoatById, saveBoat, deleteBoat, Boat } from './index';
import kakashielAxiosInstance from '../axios';

jest.mock('../axios');

describe('Boat API', () => {
	const mockBoatEntities: Boat[] = [
		{ id: 1, name: 'Boat 1' },
		{ id: 2, name: 'Boat 2' },
	];

	const mockBoat: Boat = { id: 1, name: 'Boat 1' };

	beforeEach(() => {
		(kakashielAxiosInstance.get as jest.Mock).mockClear();
		(kakashielAxiosInstance.post as jest.Mock).mockClear();
		(kakashielAxiosInstance.delete as jest.Mock).mockClear();
	});

	it('getAllBoats returns all boats', async () => {
		(kakashielAxiosInstance.get as jest.Mock).mockResolvedValue({ data: mockBoatEntities });
		const boats = await getAllBoats();
		expect(boats).toEqual(mockBoatEntities);
	});

	it('getBoatById returns a boatEntity by id', async () => {
		(kakashielAxiosInstance.get as jest.Mock).mockResolvedValue({ data: mockBoat });
		const boatEntity = await getBoatById(1);
		expect(boatEntity).toEqual(mockBoat);
	});

	it('saveBoat saves a boatEntity', async () => {
		(kakashielAxiosInstance.post as jest.Mock).mockResolvedValue({ data: mockBoat });
		const boatEntity = await saveBoat(mockBoat);
		expect(boatEntity).toEqual(mockBoat);
	});

	it('deleteBoat deletes a boatEntity', async () => {
		(kakashielAxiosInstance.delete as jest.Mock).mockResolvedValue({});
		await deleteBoat(1);
		expect(kakashielAxiosInstance.delete).toHaveBeenCalledWith('/api/v1/boats/1');
	});
});

import { AxiosInstance } from 'axios';
import kakashielAxiosInstance from './index';

jest.mock('axios', () => {
	return {
		create: jest.fn(() => ({
			get: jest.fn(),
			interceptors: {
				request: { use: jest.fn(), eject: jest.fn() },
				response: { use: jest.fn(), eject: jest.fn() },
			},
		})),
	};
});

describe('axiosInstance', () => {
	beforeEach(() => {
		jest.clearAllMocks();
		Storage.prototype.getItem = jest.fn(() => 'mock-token');
	});

	it('should send request with Authorization header', async () => {
		const instance = kakashielAxiosInstance as unknown as AxiosInstance;
		await instance.get('/test-endpoint');

		// Check if the Authorization header is set correctly
		expect(instance.get).toHaveBeenCalledWith('/test-endpoint');
	});
});

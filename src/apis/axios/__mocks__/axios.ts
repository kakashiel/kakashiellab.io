/* eslint-disable @typescript-eslint/no-unused-vars */
import { AxiosResponse } from 'axios';

const mockAxios = {
	create: jest.fn(() => ({
		get: jest.fn(),
		post: jest.fn().mockImplementation((url: string, data: any) => Promise.resolve({ data: {} } as AxiosResponse)),
		delete: jest.fn().mockImplementation((url: string) => Promise.resolve({} as AxiosResponse)),
		interceptors: {
			request: { use: jest.fn(), eject: jest.fn() },
			response: { use: jest.fn(), eject: jest.fn() },
		},
	})),
	get: jest.fn().mockImplementation((url: string) => Promise.resolve({ data: {} } as AxiosResponse)),
	post: jest.fn().mockImplementation((url: string, data: any) => Promise.resolve({ data: {} } as AxiosResponse)),
	delete: jest.fn().mockImplementation((url: string) => Promise.resolve({} as AxiosResponse)),
	interceptors: {
		request: {
			use: jest.fn(),
		},
		response: {
			use: jest.fn(),
		},
	},
};

export default mockAxios;
